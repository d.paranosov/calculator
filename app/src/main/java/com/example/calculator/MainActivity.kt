package com.example.calculator

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.DecimalFormat
import java.text.NumberFormat

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var tv1 : TextView
    private lateinit var t_edit : EditText

    private var a : Double = 0.0
    private var b : Double = 0.0
    private var op : Char = '\u0000'
    private var i : Int = 0
    private var h : Boolean = true
    private var f : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tv1 = findViewById(R.id.textView)
        t_edit = findViewById(R.id.editText)

        findViewById<Button>(R.id.button).setOnClickListener(this)
        findViewById<Button>(R.id.button2).setOnClickListener(this)
        findViewById<Button>(R.id.button3).setOnClickListener(this)
        findViewById<Button>(R.id.button4).setOnClickListener(this)
        findViewById<Button>(R.id.button5).setOnClickListener(this)
        findViewById<Button>(R.id.button6).setOnClickListener(this)
        findViewById<Button>(R.id.button7).setOnClickListener(this)
        findViewById<Button>(R.id.button8).setOnClickListener(this)
        findViewById<Button>(R.id.button9).setOnClickListener(this)
        findViewById<Button>(R.id.button10).setOnClickListener(this)
        findViewById<Button>(R.id.button11).setOnClickListener(this)
        findViewById<Button>(R.id.button12).setOnClickListener(this)
        findViewById<Button>(R.id.button13).setOnClickListener(this)
        findViewById<Button>(R.id.button14).setOnClickListener(this)
        findViewById<Button>(R.id.button15).setOnClickListener(this)
        findViewById<Button>(R.id.button16).setOnClickListener(this)
        //findViewById<Button>(R.id.button17).setOnClickListener(this)
        findViewById<Button>(R.id.button18).setOnClickListener(this)
        findViewById<Button>(R.id.button19).setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id ?: return) {
            R.id.button-> { t_edit.setText(getTextFromButton(view)) } // цифры
            R.id.button2-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button3-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button5-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button6-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button7-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button9-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button10-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button11-> { t_edit.setText(getTextFromButton(view)) }
            R.id.button14-> { t_edit.setText(getTextFromButton(view)) }

            R.id.button4-> { // разделить
                if (i == 0) { tv1.text = ""; input() }
                if (i == 2) input()
                if (!h) outputOperation('\u00F7')
                else { op = '\u00F7'
                    val str: String = tv1.text.toString() + "\n" + op.toString()
                    tv1.text = str
                    i += 1
                    f = false }
            }
            R.id.button8-> { // умножить
                if (i == 0) { tv1.text = ""; input() }
                if (i == 2) input()
                if (!h) outputOperation('\u00D7')
                else { op = '\u00D7'
                    val str: String = tv1.text.toString() + "\n" + op.toString()
                    tv1.text = str
                    i += 1
                    f = false }
            }
            R.id.button12-> { // минус
                if (i == 0) { tv1.text = ""; input() }
                if (i == 2) input()
                if (!h) outputOperation('-')
                else { op = '-'
                    val str: String = tv1.text.toString() + "\n" + op.toString()
                    tv1.text = str
                    i += 1
                    f = false }
            }
            R.id.button16-> { // плюс
                if (i == 0) { tv1.text = ""; input() }
                if (i == 2) input()
                if (!h) outputOperation('+')
                else {// else { t_edit.setText("+"); f = false }
                    op = '+'
                    val str: String = tv1.text.toString() + "\n" + op.toString()
                    tv1.text = str
                    i += 1
                    f = false
                }
            }

            R.id.button13-> { // равно
                input()
                h = true
                f = true
                t_edit.setText(getAnswer())
            }

            R.id.button15-> { t_edit.setText(getTextFromButton(view)) } // запятая

            R.id.button18-> { t_edit.setText("") } // очистить всё поле
            R.id.button19-> { t_edit.setText(t_edit.text.dropLast(1)) } // стереть
        }
    }

    private fun getTextFromButton(view: View?): String {
        if (f) { t_edit.setText(""); f = false }
        if (i == 1) input()
        var str: String = t_edit.text.toString()
        str += (view as Button).text.toString()
        return str
    }

    private fun input() {
        when (i) {
            0 -> setA()
            1 -> setOP()
            2 -> setB()
        }
        //indent()
    }

    private fun setA() {
        try {
            if (i == 0) {
                a = t_edit.text.toString().toDouble()
                i++
                indent()
            }
        } catch (nfe: NumberFormatException) {
            val text = "Введите число правильно!"
            val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
            toast.show()
            t_edit.setText("")
        }

    }

    private fun setOP() {
        try {
            if (i == 1) {
                op = t_edit.text.toString().single()
                val match = Regex("""[÷×+*/:-]""").find(op.toString())
                    ?: throw IllegalArgumentException("Enter the operation correctly!")
                i++
                indent()
            }
        } catch (nfe: IllegalArgumentException) {
            val text = "Введите операцию правильно!"
            val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
            toast.show()
            t_edit.setText("")
        } catch (nfe: NoSuchElementException) {
            val text = "Не оставляйте поле пустым!"
            val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
            toast.show()
        }
    }

    private fun setB() {
        try {
            if (i == 2) {
                b = t_edit.text.toString().toDouble()
                i = 0
                h = false
                indent()
            }
        } catch (nfe: NumberFormatException) {
            val text = "Введите число правильно!"
            val toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT)
            toast.show()
            t_edit.setText("")
        }
    }

    private fun getAnswer(): String {
        val format: NumberFormat = DecimalFormat("0.#####")
        val t: String = when (op) {
            '\u00F7',':','/'-> { (format.format(a / b)).toString() }
            '\u00D7','*'-> { (format.format(a * b)).toString() }
            '-'-> { (format.format(a - b)).toString() }
            '+'-> { (format.format(a + b)).toString() }
            else -> { "Error" }
        }
        return t
    }

    private fun indent() {
        val str: String = if (tv1.text == "") {
            t_edit.text.toString()
        } else {
            tv1.text.toString() + "\n" + t_edit.text.toString()
        }
        tv1.text = str
        t_edit.setText("")
    }

    private fun outputOperation(operation: Char) {
        tv1.text = getAnswer()
        a = tv1.text.toString().toDouble()
        op = operation
        val str: String = tv1.text.toString() + "\n" + op.toString()
        tv1.text = str
        i += 2
    }
}